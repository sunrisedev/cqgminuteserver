﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQGMinuteServer
{
    class MinuteLogger
    {
        private static Queue<String> logQueue;
        private static string logDir = ".\\";
        private static string logFile = "_MinuteBarsInfo.txt";
        private static MinuteLogger instance;

        // Private constructor to prevent instance creation
        private MinuteLogger()
        {
        }

        public static MinuteLogger Instance
        {
            get
            {
                // If the instance is null then create one and init the Queue
                if (instance == null)
                {
                    instance = new MinuteLogger();
                    logQueue = new Queue<String>();
                }

                return instance;
            }
        }//public static MinuteLogger Instance

        // The method that writes to the log file
        public void WriteToLog(string message)
        {
            try
            {
                // Lock the queue while writing to prevent contention for the log file
                lock (logQueue)
                {
                    logQueue.Enqueue(message);
                }
            }
            catch (Exception ex)
            {
                string msg = "MinuteBarEventLog.WriteToLog(ex). " + ex.Message;
                Console.WriteLine(msg);
                msg = null;
            }
        }//public void WriteToLog(string message)

        // Flushes the Queue to the physical log file
        public void FlushLog()
        {
            try
            {

                string logPath = logDir + DateTime.Now.ToString("yyyy-MM-dd") + logFile;

                using (FileStream fs = File.Open(logPath, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter log = new StreamWriter(fs))
                    {
                        while (logQueue.Count > 0)
                        {
                            String entry;
                            lock (logQueue)
                            {
                                entry = logQueue.Dequeue();
                            }
                            log.WriteLine(entry);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string msg = "MinuteBarEventLog.FlushLog(ex). " + ex.Message;
                Console.WriteLine(msg);
            }
        }//public void FlushLog()
    }//class MinuteLogger
}//namespace CQGMinuteServer
