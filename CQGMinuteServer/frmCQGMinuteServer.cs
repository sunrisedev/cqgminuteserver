﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CQGMinuteServer
{
    public partial class frmCQGMinuteServer : Form
    {
        private Boolean DEBUG = false;
        private CQG_Minute_API minutes = null;
        private MinuteLogger minute_logger = null;
        private MinutesToSQL minutes_to_sql = null;
        private ErrorLogger logger;
        private Boolean AUTO_SHUTDOWN = false;
        private TimeSpan AUTO_SHUTDOWN_TIME;
        private System.Threading.Timer tmrShutDown = null;
        private System.Threading.Timer tmrPreShutDown = null;
        private String oConStrSunTrading = null;
        private Task RunUpdates = null;
        private EmailClient email = null;
        private Boolean blnChecktxtLookBack = false;

        private String strContinuationType = null;
        private String strSessionFilter = null;
        private String strHistorical = null;
        private String strIntraDay = null;
        private String strTick = null;
        private Boolean blnDailyFromIntraday = false;
        private Boolean blnEqualizeCloses = false;
        private String strNumBars = null;
        private Int32 intNumBars = 0;

        public frmCQGMinuteServer()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.frmCQGMinuteServer(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                //else
                //{
                //    logger.WriteToLog(msg);
                //}
                msg = null;
            }
        }//public frmCQGMinuteServer()

        private void frmCQGMinuteServer_Load(object sender, EventArgs e)
        {
            try
            {
                logger = ErrorLogger.Instance;
                email = new EmailClient();

                //set defaults
                strContinuationType = ConfigurationManager.AppSettings["CONTINUATION_TYPE"].ToString();
                strSessionFilter = ConfigurationManager.AppSettings["SESSIONS_FILTER"].ToString();
                strHistorical = ConfigurationManager.AppSettings["HISTORICAL"].ToString();
                strIntraDay = ConfigurationManager.AppSettings["INTRADAY"].ToString();
                strTick = ConfigurationManager.AppSettings["TICK"].ToString();
                strNumBars = ConfigurationManager.AppSettings["BARS_TO_LOOK_BACK"].ToString();
                blnDailyFromIntraday = Convert.ToBoolean(ConfigurationManager.AppSettings["DAILY_FROM_INTRADAY"]);
                blnEqualizeCloses = Convert.ToBoolean(ConfigurationManager.AppSettings["EQUALIZE_CLOSES"]);

                intNumBars = Convert.ToInt32(strNumBars);

                chkEqualizeCloses.Checked = blnEqualizeCloses;
                chkSFDailyFromIntraday.Checked = blnDailyFromIntraday;

                cmbTickFilter.SelectedIndex = cmbTickFilter.FindStringExact(strTick);
                nudIntradayPeriod.Value = Convert.ToInt32(strIntraDay);
                cmbHistoricalPeriod.SelectedIndex = cmbHistoricalPeriod.FindStringExact(strHistorical);
                cmbSessionFilter.SelectedIndex = cmbSessionFilter.FindStringExact(strSessionFilter);
                cmbContinuationType.SelectedIndex = cmbContinuationType.FindStringExact(strContinuationType);
                txtLookBack.Text = strNumBars;

                blnChecktxtLookBack = true;

                minute_logger = MinuteLogger.Instance;
                minutes_to_sql = new MinutesToSQL();

                oConStrSunTrading = ConfigurationManager.ConnectionStrings["SunTrading"].ToString();
                String isAutoShutdown = ConfigurationManager.AppSettings["AUTO_SHUTDOWN"].ToString();
                if (isAutoShutdown.ToUpper().Equals("TRUE"))
                {
                    AUTO_SHUTDOWN = true;
                }
                else
                {
                    AUTO_SHUTDOWN = false;
                }

                if (AUTO_SHUTDOWN)
                {
                    String[] time = ConfigurationManager.AppSettings["AUTO_SHUTDOWN_TIME"].ToString().Split(':');
                    AUTO_SHUTDOWN_TIME = new TimeSpan(Convert.ToInt32(time[0]), Convert.ToInt32(time[1]), Convert.ToInt32(time[2]));

                    DateTime current = DateTime.Now;
                    TimeSpan timeToGo = AUTO_SHUTDOWN_TIME - current.TimeOfDay;
                    if (timeToGo < TimeSpan.Zero)
                    {
                        timeToGo = AUTO_SHUTDOWN_TIME.Add(new TimeSpan(1, 0, 0, 0)) - current.TimeOfDay;
                    }
                    tmrShutDown = new System.Threading.Timer(new TimerCallback(timer_ShutDown), null, timeToGo, System.Threading.Timeout.InfiniteTimeSpan);
                    tmrPreShutDown = new System.Threading.Timer(new TimerCallback(timer_getLastDataBeforeShutDown), null, timeToGo.Subtract(TimeSpan.FromMinutes(1)), System.Threading.Timeout.InfiniteTimeSpan);
                }

                String[] args = Environment.GetCommandLineArgs();
                if (args.Length > 1)
                {
                    //Console.WriteLine("AutoStart args: " + String.Join(", ", args));
                    if (args[1].ToUpper().Equals("-S"))
                    {
                        backgroundWorker1.RunWorkerAsync();

                        //StartServer();
                    }
                }

            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.frmCQGMinuteServer_Load(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void frmCQGMinuteServer_Load(object sender, EventArgs e)

        private void timer_getLastDataBeforeShutDown(Object state)
        {
            try
            {
                minutes.Resubscribe();
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.timer_getLastDataBeforeShutDown(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void timer_getLastDataBeforeShutDown(Object state)

        public void timer_ShutDown(Object state)
        {
            if (!backgroundWorker2.IsBusy)
            {
                backgroundWorker2.RunWorkerAsync();
            }
            this.BeginInvoke(new Action(Close), new object[] { });
        }//public void timer_ShutDown(Object state)

        private async Task delayUpdates()
        {
            await Task.Delay(1000);
            this.minute_logger.FlushLog();
            this.minutes_to_sql.updateSQL();
            lstCQGEvents.BeginInvoke(new update_listbox_callback(this.update_lstEvents), new object[] { "Last Update@: " + DateTime.Now.ToString("hh:mm:ss.fff tt") });
        }

        private void cmdStart_Click(object sender, EventArgs e)
        {
            try
            {
                backgroundWorker1.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.cmdStart_Click(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void cmdStart_Click(object sender, EventArgs e)

        private void cmdStop_Click(object sender, EventArgs e)
        {
            try
            {
                backgroundWorker2.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.cmdStop_Click(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void cmdStop_Click(object sender, EventArgs e)



        private void cmbSessionFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                strSessionFilter = cmbSessionFilter.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.cmbSessionFilter_SelectedIndexChanged(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void cmbSessionFilter_SelectedIndexChanged(object sender, EventArgs e)

        private void cmbHistoricalPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                strHistorical = cmbHistoricalPeriod.SelectedItem.ToString();
                nudIntradayPeriod.Enabled = ((cmbHistoricalPeriod.SelectedItem) == "hpUndefined");
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.cmbHistoricalPeriod_SelectedIndexChanged(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void cmbHistoricalPeriod_SelectedIndexChanged(object sender, EventArgs e)

        private void cmbContinuationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                strContinuationType = cmbContinuationType.SelectedItem.ToString();

                switch (cmbContinuationType.SelectedItem.ToString())
                {
                    case "tsctNoContinuation":
                        chkEqualizeCloses.Checked = false;
                        chkEqualizeCloses.Enabled = false;
                        break;
                    case "tsctStandard":
                        chkEqualizeCloses.Checked = false;
                        chkEqualizeCloses.Enabled = false;
                        break;
                    case "tsctStandardByMonth":
                        chkEqualizeCloses.Checked = false;
                        chkEqualizeCloses.Enabled = false;
                        break;
                    case "tsctActive":
                        chkEqualizeCloses.Checked = false;
                        chkEqualizeCloses.Enabled = true;
                        break;
                    case "tsctActiveByMonth":
                        chkEqualizeCloses.Checked = false;
                        chkEqualizeCloses.Enabled = true;
                        break;
                    default:
                        chkEqualizeCloses.Checked = false;
                        chkEqualizeCloses.Enabled = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.cmbContinuationType_SelectedIndexChanged(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void cmbContinuationType_SelectedIndexChanged(object sender, EventArgs e)



        private void nudIntradayPeriod_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                strIntraDay = nudIntradayPeriod.Value.ToString();
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.nudIntradayPeriod_ValueChanged(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void nudIntradayPeriod_ValueChanged(object sender, EventArgs e)


        private void cmbTickFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                strTick = cmbTickFilter.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.cmbTickFilter_SelectedIndexChanged(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void cmbTickFilter_SelectedIndexChanged(object sender, EventArgs e)

        private void chkSFDailyFromIntraday_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                blnDailyFromIntraday = chkSFDailyFromIntraday.Checked;
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.chkSFDailyFromIntraday_CheckedChanged(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void chkSFDailyFromIntraday_CheckedChanged(object sender, EventArgs e)

        private void chkEqualizeCloses_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                blnEqualizeCloses = chkEqualizeCloses.Checked;
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.chkEqualizeCloses_CheckedChanged(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void chkEqualizeCloses_CheckedChanged(object sender, EventArgs e)

        private void cmdExit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!backgroundWorker2.IsBusy)
                {
                    backgroundWorker2.RunWorkerAsync();
                }
                this.Close();
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.cmdExit_Click(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void cmdExit_Click(object sender, EventArgs e)

        private void StartServer()
        {
            try
            {
                if (email == null)
                {
                    email = new EmailClient();
                }

                if (minutes == null)
                {
                    minutes = new CQG_Minute_API(intNumBars);
                }
                minutes.Info += minutes_Info;
                minutes.NewMinute += minutes_NewMinute;
                minutes.SubscribeToMarkets(GetMarkets(oConStrSunTrading));

                
                //Set settings
                minutes.setSubscriptionSettings(false,Convert.ToInt32(strIntraDay),strTick,strContinuationType, Convert.ToInt32(strSessionFilter),blnDailyFromIntraday,blnEqualizeCloses);
                minutes.StartMinuteServer();
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.StartServer(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void StartServer()

        private void StopServer()
        {
            try
            {
                if (minutes != null)
                {
                    minutes.StopMinuteServer();
                    minutes.NewMinute -= minutes_NewMinute;
                    minutes.Info -= minutes_Info;
                }
                if (minute_logger != null)
                {
                    minute_logger.FlushLog();
                }
                if (minutes_to_sql != null)
                {
                    minutes_to_sql.updateSQL();
                }

                if (email != null)
                {
                    email = null;
                }
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.StopServer(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void StopServer()

        private void update_lstEvents(String cqg_event)
        {
            try
            {
                lstCQGEvents.Items.Add(cqg_event);
                lstCQGEvents.SelectedIndex = lstCQGEvents.Items.Count - 1;
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.update_lstEvents(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void update_lstEvents(String cqg_event)

        private void update_lstSubscribedMarkets(String subscribed_market)
        {
            try
            {
                lstSubscribedMarkets.Items.Add(subscribed_market);
                lstSubscribedMarkets.SelectedIndex = lstSubscribedMarkets.Items.Count - 1;
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.update_lstSubscribedMarkets(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void update_lstSubscribedMarkets(String subscribed_market)

        private void clear_lstSubscribedMarkets()
        {
            try
            {
                if (InvokeRequired)
                {
                    this.BeginInvoke(new Action(clear_lstSubscribedMarkets), new object[] { });
                }
                else
                {
                    lstSubscribedMarkets.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.update_lstSubscribedMarkets(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void clear_lstSubscribedMarkets()

        private void update_txtStatus(String info)
        {
            try
            {
                if (InvokeRequired)
                {
                    this.BeginInvoke(new Action<String>(update_txtStatus), new object[] { info });
                }
                else
                {
                    txtStatus.Text = info;
                }
            }
            catch (Exception e)
            {
            }
        }


        private void txtLookBack_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (blnChecktxtLookBack)
                {
                    int n;
                    bool isNumeric = int.TryParse(txtLookBack.Text, out n);
                    if (isNumeric)
                    {
                        if (n > 600)
                        {
                            MessageBox.Show("Value must be 600 or less", "Error");
                            txtLookBack.Text = strNumBars;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Value must be an integer", "Error");
                        txtLookBack.Text = strNumBars;
                    }

                    intNumBars = Convert.ToInt32(txtLookBack.Text);
                }
            }
            catch (Exception ex)
            {
            }
        }

        #region EventHandlers

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            StartServer();
        }//private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (backgroundWorker1.IsBusy)
                {
                    backgroundWorker1.CancelAsync();
                }
                StopServer();
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.backgroundWorker2_DoWork(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)

        void minutes_NewMinute(object source, MinuteBarEventArgs e)
        {
            try
            {
                minutes_to_sql.AddTick(e.getMarket(), e.getCQGTimeStamp(), e.getOpen(), e.getHigh(), e.getLow(), e.getClose(), e.getVolume(), e.getSunriseTimeStamp());
                minute_logger.WriteToLog(e.getMarket() + "," + e.getCQGTimeStamp().ToString() + "," + e.getOpen().ToString() + "," + e.getHigh().ToString() + "," + e.getLow().ToString() + "," + e.getClose().ToString() + "," + e.getVolume().ToString() + "," + e.getSunriseTimeStamp().ToString("hh:mm:ss.fff tt"));
                if (RunUpdates == null)
                {
                    RunUpdates = this.delayUpdates();
                    //RunUpdates.Start();
                }
                else if (RunUpdates.IsCompleted)
                {
                    RunUpdates = this.delayUpdates();
                }
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.minutes_NewMinute(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//void minutes_NewMinute(object source, MinuteBarEventArgs e)

        void minutes_Info(object source, InstrumentChangedEventArgs e)
        {
            try
            {
                if (e.getStatus() == InstrumentChangedEventArgs.StatusCode.INSTRUMENT_SUBSCRIBED)
                {
                    lstSubscribedMarkets.BeginInvoke(new update_listbox_callback(this.update_lstSubscribedMarkets), new object[] { e.getMessage() });
                }
                else
                {
                    lstCQGEvents.BeginInvoke(new update_listbox_callback(this.update_lstEvents), new object[] { "INFO: " + e.getMessage() });
                    if (e.getStatus() == InstrumentChangedEventArgs.StatusCode.DATA_CONNECTION_STATUS_CHANGED)
                    {
                        String subject = null;
                        String body = null;
                        logger.WriteToLog("CQG DATA CONNECTION CHANGED @ " +  e.getTimeStamp().ToString() + " NEW STATUS: " + e.getMessage());
                        if (e.getMessage().Contains("csConnectionUp"))
                        {
                            update_txtStatus(e.getMessage());
                            subject = "FROM: " + Environment.MachineName + " CQGMin is now up and running at " + DateTime.Now.ToString();
                            body = "CQG minute server is up and running";
                        }
                        else if (e.getMessage().Contains("csConnectionDown") || e.getMessage().Contains("csConnectionNotLoggedOn"))
                        {
                            update_txtStatus(e.getMessage());
                            clear_lstSubscribedMarkets();
                            subject = "FROM: " + Environment.MachineName + " CQGMin is not running at " + DateTime.Now.ToString();
                            body = "For Some reason CQG minute server is not up and running";
                        }
                        else if (e.getMessage().Contains("csConnectionTrouble"))
                        {
                            subject = "FROM: " + Environment.MachineName + " CQGMin is reporting connection trouble @ " + DateTime.Now.ToString();
                            body = "For Some reason CQG minute server is reporting: csConnectionTrouble";
                        }
                        else if (e.getMessage().Contains("csConnectionDelayed"))
                        {
                            subject = "FROM: " + Environment.MachineName + " CQGMin is reporting a delayed connection @ " + DateTime.Now.ToString();
                            body = "For Some reason CQG minute server is reporting: csConnectionDelayed";
                        }

                        if (subject != null)
                        {
                            email.SendEmail(subject, body);
                        }
                    }
                    if (e.getStatus() == InstrumentChangedEventArgs.StatusCode.DATA_ERROR)
                    {
                        StopServer();
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = "frmCQGMinuteServer.minutes_Info(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//void minutes_Info(object source, InstrumentChangedEventArgs e)

        #endregion

        #region Delegates

        public delegate void update_listbox_callback(String info);

        #endregion

        #region HelperMethods

        public List<String> GetMarkets(String connectionString)
        {
            String sql = "SELECT[symbol] FROM [SunTrading].[dbo].[v_ContractsWithStartStopTimes] WHERE [feed_name] = 'CQG API' AND (use_dde IS NULL or use_dde = 0)";
            //sql = sql + " AND id IN (117, 118, 29, 125)";
            List<String> answer = new List<string>();
            using (SqlConnection oCon = new SqlConnection(connectionString))
            {
                try
                {
                    oCon.Open();
                    using (SqlCommand oCmd = new SqlCommand(sql, oCon))
                    {
                        try
                        {
                            using (SqlDataAdapter oAdapter = new SqlDataAdapter(oCmd))
                            {
                                try
                                {
                                    using (DataTable oTable = new DataTable())
                                    {
                                        try
                                        {
                                            oAdapter.Fill(oTable);

                                            foreach (DataRow dr in oTable.Rows)
                                            {
                                                String market = null;

                                                if (!Convert.IsDBNull(dr["symbol"]))
                                                {
                                                    market = dr["symbol"].ToString();
                                                }

                                                if (market != null)
                                                {
                                                    if (!answer.Contains(market))
                                                    {
                                                        answer.Add(market);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            string msg = "frmCQGMinuteServer.GetMarkets(e). " + e.Message;
                                            if (DEBUG)
                                            {
                                                Console.WriteLine(msg);
                                            }
                                            else
                                            {
                                                logger.WriteToLog(msg);
                                            }
                                            msg = null;
                                        }
                                        finally
                                        {
                                            oTable.Clear();
                                            oTable.Dispose();
                                        }
                                    }
                                }
                                catch (Exception f)
                                {
                                    string msg = "frmCQGMinuteServer.GetMarkets(f). " + f.Message;
                                    if (DEBUG)
                                    {
                                        Console.WriteLine(msg);
                                    }
                                    else
                                    {
                                        logger.WriteToLog(msg);
                                    }
                                    msg = null;
                                }
                                finally
                                {
                                    oAdapter.Dispose();
                                }
                            }
                        }
                        catch (Exception g)
                        {
                            string msg = "frmCQGMinuteServer.GetMarkets(g). " + g.Message;
                            if (DEBUG)
                            {
                                Console.WriteLine(msg);
                            }
                            else
                            {
                                logger.WriteToLog(msg);
                            }
                            msg = null;
                        }
                        finally
                        {
                            oCmd.Dispose();
                        }
                    }
                }
                catch (Exception h)
                {
                    string msg = "frmCQGMinuteServer.GetMarkets(h). " + h.Message;
                    if (DEBUG)
                    {
                        Console.WriteLine(msg);
                    }
                    else
                    {
                        logger.WriteToLog(msg);
                    }
                    msg = null;
                }
                finally
                {
                    oCon.Close();
                    oCon.Dispose();
                }
            }
            return answer;
        }//public void GetMarkets(String connectionString)

        #endregion
    }//public partial class frmCQGMinuteServer : Form
}//namespace CQGMinuteServer
