﻿namespace CQGMinuteServer
{
    partial class frmCQGMinuteServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstSubscribedMarkets = new System.Windows.Forms.ListBox();
            this.lstCQGEvents = new System.Windows.Forms.ListBox();
            this.lblSubscribedMarkets = new System.Windows.Forms.Label();
            this.lblCQGUpdates = new System.Windows.Forms.Label();
            this.cmdExit = new System.Windows.Forms.Button();
            this.cmdStop = new System.Windows.Forms.Button();
            this.cmdStart = new System.Windows.Forms.Button();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.lblConnection = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.chkSFDailyFromIntraday = new System.Windows.Forms.CheckBox();
            this.chkEqualizeCloses = new System.Windows.Forms.CheckBox();
            this.cmbTickFilter = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.nudIntradayPeriod = new System.Windows.Forms.NumericUpDown();
            this.cmbHistoricalPeriod = new System.Windows.Forms.ComboBox();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbContinuationType = new System.Windows.Forms.ComboBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.cmbSessionFilter = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLookBack = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudIntradayPeriod)).BeginInit();
            this.GroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstSubscribedMarkets
            // 
            this.lstSubscribedMarkets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lstSubscribedMarkets.FormattingEnabled = true;
            this.lstSubscribedMarkets.Location = new System.Drawing.Point(391, 57);
            this.lstSubscribedMarkets.Name = "lstSubscribedMarkets";
            this.lstSubscribedMarkets.Size = new System.Drawing.Size(307, 264);
            this.lstSubscribedMarkets.TabIndex = 10;
            // 
            // lstCQGEvents
            // 
            this.lstCQGEvents.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lstCQGEvents.FormattingEnabled = true;
            this.lstCQGEvents.Location = new System.Drawing.Point(727, 57);
            this.lstCQGEvents.Name = "lstCQGEvents";
            this.lstCQGEvents.Size = new System.Drawing.Size(307, 264);
            this.lstCQGEvents.TabIndex = 11;
            // 
            // lblSubscribedMarkets
            // 
            this.lblSubscribedMarkets.AutoSize = true;
            this.lblSubscribedMarkets.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubscribedMarkets.Location = new System.Drawing.Point(482, 30);
            this.lblSubscribedMarkets.Name = "lblSubscribedMarkets";
            this.lblSubscribedMarkets.Size = new System.Drawing.Size(116, 15);
            this.lblSubscribedMarkets.TabIndex = 12;
            this.lblSubscribedMarkets.Text = "Subscribed Markets";
            // 
            // lblCQGUpdates
            // 
            this.lblCQGUpdates.AutoSize = true;
            this.lblCQGUpdates.Location = new System.Drawing.Point(834, 30);
            this.lblCQGUpdates.Name = "lblCQGUpdates";
            this.lblCQGUpdates.Size = new System.Drawing.Size(73, 13);
            this.lblCQGUpdates.TabIndex = 13;
            this.lblCQGUpdates.Text = "CQG Updates";
            // 
            // cmdExit
            // 
            this.cmdExit.Location = new System.Drawing.Point(271, 334);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(114, 23);
            this.cmdExit.TabIndex = 16;
            this.cmdExit.Text = "Exit";
            this.cmdExit.UseVisualStyleBackColor = true;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // cmdStop
            // 
            this.cmdStop.Location = new System.Drawing.Point(142, 334);
            this.cmdStop.Name = "cmdStop";
            this.cmdStop.Size = new System.Drawing.Size(114, 23);
            this.cmdStop.TabIndex = 15;
            this.cmdStop.Text = "Stop";
            this.cmdStop.UseVisualStyleBackColor = true;
            this.cmdStop.Click += new System.EventHandler(this.cmdStop_Click);
            // 
            // cmdStart
            // 
            this.cmdStart.Location = new System.Drawing.Point(12, 334);
            this.cmdStart.Name = "cmdStart";
            this.cmdStart.Size = new System.Drawing.Size(114, 23);
            this.cmdStart.TabIndex = 14;
            this.cmdStart.Text = "Start";
            this.cmdStart.UseVisualStyleBackColor = true;
            this.cmdStart.Click += new System.EventHandler(this.cmdStart_Click);
            // 
            // txtStatus
            // 
            this.txtStatus.Enabled = false;
            this.txtStatus.Location = new System.Drawing.Point(30, 57);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(134, 20);
            this.txtStatus.TabIndex = 17;
            // 
            // lblConnection
            // 
            this.lblConnection.AutoSize = true;
            this.lblConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnection.Location = new System.Drawing.Point(43, 30);
            this.lblConnection.Name = "lblConnection";
            this.lblConnection.Size = new System.Drawing.Size(106, 15);
            this.lblConnection.TabIndex = 18;
            this.lblConnection.Text = "Connection Status";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // backgroundWorker2
            // 
            this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);
            // 
            // chkSFDailyFromIntraday
            // 
            this.chkSFDailyFromIntraday.CheckAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.chkSFDailyFromIntraday.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkSFDailyFromIntraday.Location = new System.Drawing.Point(12, 301);
            this.chkSFDailyFromIntraday.Name = "chkSFDailyFromIntraday";
            this.chkSFDailyFromIntraday.Size = new System.Drawing.Size(145, 20);
            this.chkSFDailyFromIntraday.TabIndex = 19;
            this.chkSFDailyFromIntraday.Text = "Daily From Intraday";
            this.chkSFDailyFromIntraday.CheckedChanged += new System.EventHandler(this.chkSFDailyFromIntraday_CheckedChanged);
            // 
            // chkEqualizeCloses
            // 
            this.chkEqualizeCloses.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkEqualizeCloses.Location = new System.Drawing.Point(201, 296);
            this.chkEqualizeCloses.Name = "chkEqualizeCloses";
            this.chkEqualizeCloses.Size = new System.Drawing.Size(134, 25);
            this.chkEqualizeCloses.TabIndex = 20;
            this.chkEqualizeCloses.Text = "Equalize Closes";
            this.chkEqualizeCloses.CheckedChanged += new System.EventHandler(this.chkEqualizeCloses_CheckedChanged);
            // 
            // cmbTickFilter
            // 
            this.cmbTickFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTickFilter.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbTickFilter.Items.AddRange(new object[] {
            "tfAll",
            "tfAsk",
            "tfAskLow",
            "tfBid",
            "tfBidHigh",
            "tfDefault",
            "tfSettlement",
            "tfTrade"});
            this.cmbTickFilter.Location = new System.Drawing.Point(80, 88);
            this.cmbTickFilter.Name = "cmbTickFilter";
            this.cmbTickFilter.Size = new System.Drawing.Size(128, 24);
            this.cmbTickFilter.TabIndex = 5;
            this.cmbTickFilter.SelectedIndexChanged += new System.EventHandler(this.cmbTickFilter_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(8, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 24);
            this.label9.TabIndex = 4;
            this.label9.Text = "Tick Filter";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label7
            // 
            this.Label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label7.Location = new System.Drawing.Point(8, 24);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(64, 24);
            this.Label7.TabIndex = 0;
            this.Label7.Text = "Historical";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label6
            // 
            this.Label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label6.Location = new System.Drawing.Point(8, 56);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(64, 24);
            this.Label6.TabIndex = 2;
            this.Label6.Text = "Intraday";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nudIntradayPeriod
            // 
            this.nudIntradayPeriod.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nudIntradayPeriod.Location = new System.Drawing.Point(80, 56);
            this.nudIntradayPeriod.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nudIntradayPeriod.Name = "nudIntradayPeriod";
            this.nudIntradayPeriod.Size = new System.Drawing.Size(128, 22);
            this.nudIntradayPeriod.TabIndex = 3;
            this.nudIntradayPeriod.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudIntradayPeriod.ValueChanged += new System.EventHandler(this.nudIntradayPeriod_ValueChanged);
            // 
            // cmbHistoricalPeriod
            // 
            this.cmbHistoricalPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHistoricalPeriod.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbHistoricalPeriod.Items.AddRange(new object[] {
            "hpDaily",
            "hpMonthly",
            "hpQuarterly",
            "hpSemiannual",
            "hpUndefined",
            "hpWeekly",
            "hpYearly"});
            this.cmbHistoricalPeriod.Location = new System.Drawing.Point(80, 24);
            this.cmbHistoricalPeriod.Name = "cmbHistoricalPeriod";
            this.cmbHistoricalPeriod.Size = new System.Drawing.Size(128, 24);
            this.cmbHistoricalPeriod.TabIndex = 1;
            this.cmbHistoricalPeriod.SelectedIndexChanged += new System.EventHandler(this.cmbHistoricalPeriod_SelectedIndexChanged);
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.cmbHistoricalPeriod);
            this.GroupBox3.Controls.Add(this.nudIntradayPeriod);
            this.GroupBox3.Controls.Add(this.Label6);
            this.GroupBox3.Controls.Add(this.Label7);
            this.GroupBox3.Controls.Add(this.label9);
            this.GroupBox3.Controls.Add(this.cmbTickFilter);
            this.GroupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GroupBox3.Location = new System.Drawing.Point(12, 164);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(214, 120);
            this.GroupBox3.TabIndex = 21;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "TimedBar Parameters";
            // 
            // cmbContinuationType
            // 
            this.cmbContinuationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContinuationType.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbContinuationType.Items.AddRange(new object[] {
            "tsctActive",
            "tsctActiveByMonth",
            "tsctAdjusted",
            "tsctAdjustedByMonth",
            "tsctNoContinuation",
            "tsctStandard",
            "tsctStandardByMonth"});
            this.cmbContinuationType.Location = new System.Drawing.Point(23, 119);
            this.cmbContinuationType.Name = "cmbContinuationType";
            this.cmbContinuationType.Size = new System.Drawing.Size(152, 24);
            this.cmbContinuationType.TabIndex = 23;
            this.cmbContinuationType.SelectedIndexChanged += new System.EventHandler(this.cmbContinuationType_SelectedIndexChanged);
            // 
            // Label5
            // 
            this.Label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label5.Location = new System.Drawing.Point(27, 92);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(144, 24);
            this.Label5.TabIndex = 24;
            this.Label5.Text = "Continuation Type";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbSessionFilter
            // 
            this.cmbSessionFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSessionFilter.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbSessionFilter.Items.AddRange(new object[] {
            "0",
            "31"});
            this.cmbSessionFilter.Location = new System.Drawing.Point(208, 119);
            this.cmbSessionFilter.Name = "cmbSessionFilter";
            this.cmbSessionFilter.Size = new System.Drawing.Size(134, 24);
            this.cmbSessionFilter.TabIndex = 25;
            this.cmbSessionFilter.SelectedIndexChanged += new System.EventHandler(this.cmbSessionFilter_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(198, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 24);
            this.label1.TabIndex = 26;
            this.label1.Text = "Sessions Filter";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(229, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 15);
            this.label2.TabIndex = 28;
            this.label2.Text = "#Bars to Look Back";
            // 
            // txtLookBack
            // 
            this.txtLookBack.Location = new System.Drawing.Point(208, 57);
            this.txtLookBack.Name = "txtLookBack";
            this.txtLookBack.Size = new System.Drawing.Size(134, 20);
            this.txtLookBack.TabIndex = 29;
            this.txtLookBack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLookBack.TextChanged += new System.EventHandler(this.txtLookBack_TextChanged);
            // 
            // frmCQGMinuteServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 379);
            this.Controls.Add(this.txtLookBack);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbSessionFilter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbContinuationType);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.GroupBox3);
            this.Controls.Add(this.chkEqualizeCloses);
            this.Controls.Add(this.chkSFDailyFromIntraday);
            this.Controls.Add(this.lblConnection);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.cmdExit);
            this.Controls.Add(this.cmdStop);
            this.Controls.Add(this.cmdStart);
            this.Controls.Add(this.lblCQGUpdates);
            this.Controls.Add(this.lblSubscribedMarkets);
            this.Controls.Add(this.lstCQGEvents);
            this.Controls.Add(this.lstSubscribedMarkets);
            this.Name = "frmCQGMinuteServer";
            this.Text = "CQG Minute Server";
            this.Load += new System.EventHandler(this.frmCQGMinuteServer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudIntradayPeriod)).EndInit();
            this.GroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstSubscribedMarkets;
        private System.Windows.Forms.ListBox lstCQGEvents;
        private System.Windows.Forms.Label lblSubscribedMarkets;
        private System.Windows.Forms.Label lblCQGUpdates;
        private System.Windows.Forms.Button cmdExit;
        private System.Windows.Forms.Button cmdStop;
        private System.Windows.Forms.Button cmdStart;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Label lblConnection;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        internal System.Windows.Forms.CheckBox chkSFDailyFromIntraday;
        internal System.Windows.Forms.CheckBox chkEqualizeCloses;
        internal System.Windows.Forms.ComboBox cmbTickFilter;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.NumericUpDown nudIntradayPeriod;
        internal System.Windows.Forms.ComboBox cmbHistoricalPeriod;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.ComboBox cmbContinuationType;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.ComboBox cmbSessionFilter;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLookBack;
    }
}

