﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CQGMinuteServer
{
    class EmailClient
    {

        private String emailDistribution = null;
        private String EmailFrom = null;
        private String EmailServer = null;
        private ErrorLogger logger;
        private Boolean DEBUG = false;

        public EmailClient()
        {
            try
            {
                logger = ErrorLogger.Instance;
                emailDistribution = ConfigurationManager.AppSettings["DistributionList"].ToString();
                EmailFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                EmailServer = ConfigurationManager.AppSettings["ExchangeOrSMTP"].ToString();
            }
            catch (Exception ex)
            {
                string msg = "EmailClient.EmailClient(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//public EmailClient()

        public void SendEmail(String Subject, String Body)
        {
            try
            {
                string subject = Subject;
                SmtpClient sMail = new SmtpClient(EmailServer);
                sMail.DeliveryMethod = SmtpDeliveryMethod.Network;
                sMail.Send(EmailFrom, emailDistribution, subject, subject + Environment.NewLine + Body);
            }
            catch (Exception ex)
            {
                string msg = "EmailClient.SendEmail(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }
    }//class EmailClient
}//namespace CQGMinuteServer
