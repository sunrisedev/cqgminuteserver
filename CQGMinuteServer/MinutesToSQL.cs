﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQGMinuteServer
{
    class MinutesToSQL
    {
        private static MinutesToSQL instance;
        private static DataTable oMinutesFlush;
        private static DataTable oMinutesReceive;
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["TickWarehouse"].ToString();
        private ErrorLogger logger;

        public MinutesToSQL()
        {
            try
            {
                logger = ErrorLogger.Instance;
                oMinutesReceive = makeTicksTable();
            }
            catch (Exception ex)
            {
                String strErr = "MinutesToSQL.MinutesToSQL(ex): " + ex.ToString() + " : " + ex.Message;

                logger.WriteToLog(strErr);
            }
        }

        public void updateSQL()
        {
            var stopWatch = new Stopwatch();

            try
            {

                stopWatch.Start();

                if (oMinutesReceive.Rows.Count > 0)
                {
                    //using (SqlConnection oConn = new SqlConnection(ConnectionString))
                    //{
                    try
                    {
                        //if (oConn.State != ConnectionState.Open)
                        //{
                        //    oConn.Open();
                        //}
                        using (SqlConnection oConn = new SqlConnection(ConnectionString))
                        {
                            if (oConn.State != ConnectionState.Open)
                            {
                                oConn.Open();
                            }
                            using (SqlCommand oCommand = new SqlCommand("sp_ReceiveCQGOnMinute", oConn))
                            {

                                oMinutesFlush = oMinutesReceive;
                                lock (oMinutesReceive)
                                {
                                    oMinutesReceive = oMinutesFlush.Clone();
                                }

                                oCommand.CommandType = CommandType.StoredProcedure;
                                oCommand.Parameters.Add("@CQGOnMinute", SqlDbType.Structured);
                                oCommand.Parameters["@CQGOnMinute"].Value = oMinutesFlush;
                                //oCommand.CommandTimeout = Timeout;
                                oCommand.ExecuteNonQuery();
                                //oTicksFlush.Clear();
                                //oTicksFlush.Dispose();
                            }
                            
                            oConn.Close();
                            oConn.Dispose();
                        }
                    }
                    catch (Exception e)
                    {

                        String strErr = "MinutesToSQL.updateSQL(e): " + e.ToString() + " : " + e.Message;

                        logger.WriteToLog(strErr);

                        lock (oMinutesReceive)
                        {
                            oMinutesReceive.Merge(oMinutesFlush);
                        }
                        //oTicksFlush.Clear();
                        //oTicksFlush.Dispose();
                    }
                    finally
                    {
                        oMinutesFlush.Clear();
                        oMinutesFlush.Dispose();
                    }
                    //}

                }
            }
            catch (Exception ex)
            {
                String strErr = "MinutesToSQL.updateSQL(ex): " + ex.ToString() + " : " + ex.Message;
                logger.WriteToLog(strErr);
            }
            finally
            {
                try
                {
                    stopWatch.Stop();
                    TimeSpan executionTime = stopWatch.Elapsed;
                    logger.WriteToLog("MinutesToSQL.updateSQL(): update to SQL server took " + executionTime.ToString());
                }
                catch (Exception e)
                {
                    String strErr = "MinutesToSQL.updateSQL(e): " + e.ToString() + " : " + e.Message;
                    logger.WriteToLog(strErr);
                }
            }
            stopWatch = null;
        }//private void updateSQL()

        public DataTable makeTicksTable()
        {
            DataTable answer = new DataTable("CQGAPIOnMinuteTVP");
            answer.Columns.Add("MarketSymbol", typeof(string));
            answer.Columns.Add("DTG_Min", typeof(DateTime));
            answer.Columns.Add("OPEN", typeof(double));
            answer.Columns.Add("HIGH", typeof(double));
            answer.Columns.Add("LOW", typeof(double));
            answer.Columns.Add("CLOSE", typeof(double));
            answer.Columns.Add("Volume", typeof(int));
            answer.Columns.Add("SunriseTS", typeof(DateTime));
            return answer;
        }//public DataTable makeTicksTable()

        public void AddTick(String market, DateTime MinuteOfBar, double open, double high, double low, double close, int volume, DateTime SunriseTS)
        {
            try
            {
                lock (oMinutesReceive)
                {
                    oMinutesReceive.Rows.Add(market, MinuteOfBar, open, high, low, close, volume, SunriseTS);
                }
            }
            catch (Exception ex)
            {
                String strErr = "MinutesToSQL.AddTick(ex): " + ex.ToString() + " : " + ex.Message + ", Values are: " + market + "," + open.ToString() + ", " + high.ToString() + ", ";
                strErr = strErr + low.ToString() + ", " + close.ToString() + ", " + volume.ToString() + ", " + SunriseTS.ToString();
                logger.WriteToLog(strErr);
            }
        }//public void AddTick(String market, double price, int volume, DateTime exchangeTS, DateTime cqgTS, DateTime sunriseTS, DateTime doneTS, string isValid, long seqNum, string name, string tradetype)
    }
}
