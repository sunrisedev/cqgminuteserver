﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CQG;
using System.Configuration;
using System.Collections;

namespace CQGMinuteServer
{
    public class MinuteBarEventArgs : EventArgs
    {
        private String market;
        private Double open;
        private Double high;
        private Double low;
        private Double close;
        private Int32 volume;
        private DateTime cqgTimeStamp;
        private DateTime sunriseTimeStamp;



        public MinuteBarEventArgs(String Market, DateTime CQGTimeStamp, DateTime SunriseTimeStamp, Double Open, Double Hi, Double Lo, Double Close, Int32 Volume)
        {
            market = Market;
            open = Open;
            high = Hi;
            low = Lo;
            close = Close;
            volume = Volume;
            cqgTimeStamp = CQGTimeStamp;
            sunriseTimeStamp = SunriseTimeStamp;
        }

        public DateTime getCQGTimeStamp()
        {
            return cqgTimeStamp;
        }

        public DateTime getSunriseTimeStamp()
        {
            return sunriseTimeStamp;
        }

        public String getMarket()
        {
            return market;
        }

        public Double getOpen()
        {
            return open;
        }

        public Double getHigh()
        {
            return high;
        }

        public Double getLow()
        {
            return low;
        }

        public Double getClose()
        {
            return close;
        }

        public Int32 getVolume()
        {
            return volume;
        }
    }

    public class InstrumentChangedEventArgs : EventArgs
    {
        public enum MessageType { Info, Error };
        public enum StatusCode
        {
            INSTRUMENT_SUBSCRIBED,
            INCORRECT_SYMBOL,
            CEL_STARTED,
            IS_READY,
            DATA_ERROR,
            DATA_CONNECTION_STATUS_CHANGED,
            DDE_DISCONNECTED
        };

        private MessageType message_type;
        private String message;
        private DateTime timestamp;
        private StatusCode status;


        public InstrumentChangedEventArgs(MessageType msg, StatusCode status_code, String info, DateTime TimeStamp)
        {
            message_type = msg;
            message = info;
            timestamp = TimeStamp;
            status = status_code;
        }

        public MessageType getMessageType()
        {
            return message_type;
        }

        public String getMessage()
        {
            return message;
        }

        public DateTime getTimeStamp()
        {
            return timestamp;
        }

        public StatusCode getStatus()
        {
            return status;
        }
    }//public class InstrumentChangedEventArgs : EventArgs

    public delegate void CQGMinuteBarEvent(Object source, MinuteBarEventArgs e);
    public delegate void CQGInfoEvent(Object source, InstrumentChangedEventArgs e);

    class CQG_Minute_API
    {
        private Boolean DEBUG = false;
        private const int DEF_RANGE_START = 0;
        private int DEF_RANGE_END = -60;

        private  CQGTimedBars TimedBars;

        private Hashtable MarketsToID;
        private Hashtable IDsToMarket;
        private ErrorLogger logger;
        List<string> MarketsToSubscribeTo = null;
        //CQG Flag
        private Boolean ready = false;

        //CQG Object
        private CQGCEL CEL;

        // Current request
        //private CQGTimedBarsRequest m_TimedBarsRequest;

        public event CQGMinuteBarEvent NewMinute;
        public event CQGInfoEvent Info;

        //subscription Settings
        private Boolean includeEnd = false;
        private Int32 intradayPeriod = 1;
        private eTickFilter tickFilter = eTickFilter.tfDefault;
        private eTimeSeriesContinuationType continuation = eTimeSeriesContinuationType.tsctNoContinuation;
        private Int32 sessionFilter = 31;
        private eSessionFlag sessionFlags = eSessionFlag.sfUndefined;
        private Boolean equalizeCloses = false;
        private eHistoricalPeriod historicalPeriod = eHistoricalPeriod.hpUndefined;

        public CQG_Minute_API(int lookback)
        {
            try
            {
                logger = ErrorLogger.Instance;

                DEF_RANGE_END = lookback * -1;

                CEL = new CQGCEL();

                CEL.TimedBarsResolved += new CQG._ICQGCELEvents_TimedBarsResolvedEventHandler(CEL_TimedBarsResolved);
                CEL.DataConnectionStatusChanged += new CQG._ICQGCELEvents_DataConnectionStatusChangedEventHandler(CEL_DataConnectionStatusChanged);
                CEL.TimedBarsAdded += new CQG._ICQGCELEvents_TimedBarsAddedEventHandler(CEL_TimedBarsAdded);

                CEL.APIConfiguration.ReadyStatusCheck = eReadyStatusCheck.rscOff;
                CEL.APIConfiguration.CollectionsThrowException = false;

                MarketsToID = null;
                IDsToMarket = null;
                MarketsToSubscribeTo = null;
            }
            catch (Exception ex)
            {
                string msg = "CQG_Minute_API.CQG_Minute_API(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//public CQG_Minute_API()

        public void setSubscriptionSettings(bool include_end, Int32 intraday_period, String tick_filter, String continuation_type, Int32 session_filter, Boolean session_flags, bool equalize_closes)
        {
            try
            {
                includeEnd = include_end;

                if (intraday_period > 0)
                {
                    intradayPeriod = intraday_period;
                }

                switch (tick_filter)
                {
                    case "tfAll":
                        tickFilter = eTickFilter.tfAll;
                        break;
                    case "tfAsk":
                        tickFilter = eTickFilter.tfAsk;
                        break;
                    case "tfAskLow":
                        tickFilter = eTickFilter.tfAskLow;
                        break;
                    case "tfBid":
                        tickFilter = eTickFilter.tfBid;
                        break;
                    case "tfBidHigh":
                        tickFilter = eTickFilter.tfBidHigh;
                        break;
                    case "tfDefault":
                        tickFilter = eTickFilter.tfDefault;
                        break;
                    case "tfSettlement":
                        tickFilter = eTickFilter.tfSettlement;
                        break;
                    case "tfTrade":
                        tickFilter = eTickFilter.tfTrade;
                        break;
                }

                switch (continuation_type)
                {
                    case "tsctActive":
                        continuation = eTimeSeriesContinuationType.tsctActive;
                        break;
                    case "tsctActiveByMonth":
                        continuation = eTimeSeriesContinuationType.tsctActiveByMonth;
                        break;
                    case "tsctAdjusted":
                        continuation = eTimeSeriesContinuationType.tsctAdjusted;
                        break;
                    case "tsctAdjustedByMonth":
                        continuation = eTimeSeriesContinuationType.tsctAdjustedByMonth;
                        break;
                    case "tsctNoContinuation":
                        continuation = eTimeSeriesContinuationType.tsctNoContinuation;
                        break;
                    case "tsctStandard":
                        continuation = eTimeSeriesContinuationType.tsctStandard;
                        break;
                    case "tsctStandardByMonth":
                        continuation = eTimeSeriesContinuationType.tsctStandardByMonth;
                        break;
                }

                sessionFilter = session_filter;

                switch (session_flags)
                {
                    case true:
                        sessionFlags = eSessionFlag.sfDailyFromIntraday;
                        break;
                    case false:
                        sessionFlags = eSessionFlag.sfUndefined;
                        break;
                }

                equalizeCloses = equalize_closes;
            }
            catch (Exception ex)
            {
                string msg = "CQG_Minute_API.setSubscriptionSettings(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }

        public void SubscribeToMarkets(List<String> markets)
        {
            try
            {

                MarketsToSubscribeTo = markets;
                MarketsToID = new Hashtable(markets.Count * 3, 0.5f);
                IDsToMarket = new Hashtable(markets.Count * 3, 0.5f);

                //foreach (String s in markets)
                //{
                //    MarketsToID.Add(s, null);
                //}
            }
            catch (Exception ex)
            {
                string msg = "CQG_Minute_API.SubscribeToMarkets(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//public void SubscribeToMarkets(List<String> markets)

        public void StartMinuteServer()
        {
            try
            {
                if (MarketsToID != null)
                {
                    CEL.Startup();
                }
                else
                {
                    string msg = "CQG_Minute_API.StartMinuteServer(): you tried to start the server with no subscription list";
                    if (DEBUG)
                    {
                        Console.WriteLine(msg);
                    }
                    else
                    {
                        logger.WriteToLog(msg);
                    }
                    msg = null;
                }
            }
            catch (Exception ex)
            {
                string msg = "CQG_Minute_API.StartMinuteServer(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//public void StartMinuteServer()

        public void Resubscribe()
        {
            try
            {
                CEL.RemoveAllTimedBars();
                IDsToMarket.Clear();
                //Subscribe to markets here
                foreach (String s in MarketsToSubscribeTo)
                {
                    CQGTimedBarsRequest m_TimedBarsRequest = CEL.CreateTimedBarsRequest();
                    m_TimedBarsRequest.Symbol = s;
                    m_TimedBarsRequest.RangeStart = DEF_RANGE_START;
                    m_TimedBarsRequest.RangeEnd = DEF_RANGE_END;
                    m_TimedBarsRequest.IncludeEnd = includeEnd;

                    if (historicalPeriod == eHistoricalPeriod.hpUndefined)
                    {
                        m_TimedBarsRequest.IntradayPeriod = intradayPeriod; //one minute bars
                    }
                    else
                    {
                        m_TimedBarsRequest.HistoricalPeriod = historicalPeriod;
                    }

                    m_TimedBarsRequest.TickFilter = tickFilter;
                    m_TimedBarsRequest.Continuation = continuation;
                    m_TimedBarsRequest.UpdatesEnabled = true;
                    m_TimedBarsRequest.SessionsFilter = sessionFilter;
                    m_TimedBarsRequest.SessionFlags = sessionFlags;
                    m_TimedBarsRequest.EqualizeCloses = equalizeCloses;
                    m_TimedBarsRequest.ExcludeAllOutputs();
                    m_TimedBarsRequest.IncludeOutput(eTimedBarsRequestOutputs.tbrActualVolume, true);

                    TimedBars = CEL.RequestTimedBars(m_TimedBarsRequest);

                    IDsToMarket.Add(TimedBars.Id, s);
                    //MarketsToID.Add(s, TimedBars);
                    if (MarketsToID.ContainsKey(s))
                    {
                        MarketsToID[s] = TimedBars;
                    }
                    else
                    {
                        MarketsToID.Add(s, TimedBars);
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = "CQG_Minute_API.Resubscribe(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }//public void Resubscribe()

        public void StopMinuteServer()
        {
            try
            {
                CEL.RemoveAllTimedBars();
                CEL.Shutdown();
            }
            catch (Exception ex)
            {
                string msg = "CQG_Minute_API.StopMinuteServer(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }

        private void CEL_TimedBarsAdded(CQGTimedBars cqg_timed_bars)
        {
            try
            {
                if (cqg_timed_bars.Status == eRequestStatus.rsSuccess)
                {
                    //Console.WriteLine("Timed Bars Added");
                    
                    if (IDsToMarket.ContainsKey(cqg_timed_bars.Id))
                    {
                        String Market = Market = IDsToMarket[cqg_timed_bars.Id].ToString();
                        Double open, high, low, close;
                        Int32 vol;
                        DateTime Timestamp;
                        Int32 index = cqg_timed_bars.Count - 2;
                        if (index >= 0)
                        {
                            vol = cqg_timed_bars[index].ActualVolume;
                            if (vol > 0)
                            {
                                open = cqg_timed_bars[index].Open;
                                high = cqg_timed_bars[index].High;
                                low = cqg_timed_bars[index].Low;
                                close = cqg_timed_bars[index].Close;
                                Timestamp = cqg_timed_bars[index].Timestamp.ToLocalTime();

                                //Console.WriteLine("NEW_BAR, " + Timestamp.ToString() + ", O:" + open.ToString() + ", H:" + high.ToString() + ", L:" + low.ToString() + ", C:" + close.ToString() + ", V:" + vol.ToString());
                                //minute_log.WriteToLog(Market + ",NEW_BAR," + Timestamp.ToString() + "," + open.ToString() + "," + high.ToString() + "," + low.ToString() + "," + close.ToString() + "," + vol.ToString() + "," + DateTime.Now.ToString("hh:mm:ss.fff tt"));
                                //minute_log.FlushLog();
                                OnMinuteUpdate(new MinuteBarEventArgs(Market, Timestamp, DateTime.Now, open, high, low, close, vol));
                            }
                        }
                        //index = index - 1;
                        //if (index >= 0)
                        //{
                        //    vol = cqg_timed_bars[index].ActualVolume;
                        //    if (vol > 0)
                        //    {
                        //        open = cqg_timed_bars[index].Open;
                        //        high = cqg_timed_bars[index].High;
                        //        low = cqg_timed_bars[index].Low;
                        //        close = cqg_timed_bars[index].Close;
                        //        Timestamp = cqg_timed_bars[index].Timestamp.ToLocalTime();

                        //        //Console.WriteLine("NEW_BAR, " + Timestamp.ToString() + ", O:" + open.ToString() + ", H:" + high.ToString() + ", L:" + low.ToString() + ", C:" + close.ToString() + ", V:" + vol.ToString());
                        //        //minute_log.WriteToLog(Market + ",NEW_BAR," + Timestamp.ToString() + "," + open.ToString() + "," + high.ToString() + "," + low.ToString() + "," + close.ToString() + "," + vol.ToString() + "," + DateTime.Now.ToString("hh:mm:ss.fff tt"));
                        //        //minute_log.FlushLog();
                        //        OnMinuteUpdate(new MinuteBarEventArgs(Market, Timestamp, DateTime.Now, open, high, low, close, vol));
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = "Program.CEL_TimedBarsAdded(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }

        private void CEL_DataConnectionStatusChanged(eConnectionStatus new_status)
        {
            //Console.WriteLine("Data Connection Status Changed status is: " + new_status.ToString());
            try
            {
                OnStatusUpdate(new InstrumentChangedEventArgs(InstrumentChangedEventArgs.MessageType.Info, InstrumentChangedEventArgs.StatusCode.DATA_CONNECTION_STATUS_CHANGED, new_status.ToString(), DateTime.Now));
                if (new_status == eConnectionStatus.csConnectionUp)
                {
                    Resubscribe();
                    ////Subscribe to markets here
                    //foreach (String s in MarketsToSubscribeTo)
                    //{
                    //    CQGTimedBarsRequest m_TimedBarsRequest = CEL.CreateTimedBarsRequest();
                    //    m_TimedBarsRequest.Symbol = s;
                    //    m_TimedBarsRequest.RangeStart = DEF_RANGE_START;
                    //    m_TimedBarsRequest.RangeEnd = DEF_RANGE_END;
                    //    m_TimedBarsRequest.IncludeEnd = includeEnd;

                    //    if (historicalPeriod == eHistoricalPeriod.hpUndefined)
                    //    {
                    //        m_TimedBarsRequest.IntradayPeriod = intradayPeriod; //one minute bars
                    //    }
                    //    else
                    //    {
                    //        m_TimedBarsRequest.HistoricalPeriod = historicalPeriod;
                    //    }

                    //    m_TimedBarsRequest.TickFilter = tickFilter;
                    //    m_TimedBarsRequest.Continuation = continuation;
                    //    m_TimedBarsRequest.UpdatesEnabled = true;
                    //    m_TimedBarsRequest.SessionsFilter = sessionFilter;
                    //    m_TimedBarsRequest.SessionFlags = sessionFlags;
                    //    m_TimedBarsRequest.EqualizeCloses = equalizeCloses;
                    //    m_TimedBarsRequest.ExcludeAllOutputs();
                    //    m_TimedBarsRequest.IncludeOutput(eTimedBarsRequestOutputs.tbrActualVolume, true);

                    //    TimedBars = CEL.RequestTimedBars(m_TimedBarsRequest);

                    //    IDsToMarket.Add(TimedBars.Id, s);
                    //    if (MarketsToID.ContainsKey(s))
                    //    {
                    //        MarketsToID[s] = TimedBars;
                    //    }
                    //    else
                    //    {
                    //        MarketsToID.Add(s, TimedBars);
                    //    }
                    //
                    //}
                }
            }
            catch (Exception ex)
            {
                string msg = "CQG_Minute_API.CEL_DataConnectionStatusChanged(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }

        private void CEL_TimedBarsResolved(CQGTimedBars cqg_timed_bars, CQGError cqg_error)
        {
            //Console.WriteLine("Timed Bars Resolveded");
            try
            {
                if (IDsToMarket.ContainsKey(cqg_timed_bars.Id))
                {
                    String Market = null;
                    Market = IDsToMarket[cqg_timed_bars.Id].ToString();
                    OnStatusUpdate(new InstrumentChangedEventArgs(InstrumentChangedEventArgs.MessageType.Info, InstrumentChangedEventArgs.StatusCode.INSTRUMENT_SUBSCRIBED, "Instrument Data Subscribed : " + Market, DateTime.Now));

                    for (int i = 0; i < cqg_timed_bars.Count; i++)
                    {
                        if (cqg_timed_bars[i].ActualVolume > 0)
                        {
                            Double open, high, low, close;
                            Int32 vol = cqg_timed_bars[i].ActualVolume;
                            DateTime Timestamp = cqg_timed_bars[i].Timestamp.ToLocalTime();

                            open = cqg_timed_bars[i].Open;
                            high = cqg_timed_bars[i].High;
                            low = cqg_timed_bars[i].Low;
                            close = cqg_timed_bars[i].Close;

                            //Console.WriteLine("NEW_BAR, " + Timestamp.ToString() + ", O:" + open.ToString() + ", H:" + high.ToString() + ", L:" + low.ToString() + ", C:" + close.ToString() + ", V:" + vol.ToString());
                            //minute_log.WriteToLog(Market + ",NEW_BAR," + Timestamp.ToString() + "," + open.ToString() + "," + high.ToString() + "," + low.ToString() + "," + close.ToString() + "," + vol.ToString() + "," + DateTime.Now.ToString("hh:mm:ss.fff tt"));
                            //minute_log.FlushLog();
                            OnMinuteUpdate(new MinuteBarEventArgs(Market, Timestamp, DateTime.Now, open, high, low, close, vol));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = "CQG_Minute_API.CEL_TimedBarsResolved(ex). " + ex.Message;
                if (DEBUG)
                {
                    Console.WriteLine(msg);
                }
                else
                {
                    logger.WriteToLog(msg);
                }
                msg = null;
            }
        }


        protected virtual void OnMinuteUpdate(MinuteBarEventArgs e)
        {
            var x = NewMinute;
            if (x != null)
            {
                NewMinute(this, e);
            }
        }//protected virtual void OnTickUpdate(PriceChangedEventArgs e)

        protected virtual void OnStatusUpdate(InstrumentChangedEventArgs e)
        {
            var x = Info;
            if (x != null)
            {
                Info(this, e);
            }
        }//protected virtual void OnStatusUpdate(InstrumentChangedEventArgs e)
    }
}
