﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQGMinuteServer
{
    class ErrorLogger
    {
        private static ErrorLogger instance;
        private static Queue<ErrorLog> logQueue;
        private static string logDir = "";
        private static string logFile = "Errors.txt";
        private static int maxLogAge = int.Parse("10"); //Time in seconds for flushing the queue to log
        private static int queueSize = int.Parse("5"); // Size of queue before it flushes
        private static DateTime LastFlushed = DateTime.Now;


        // Private constructor to prevent instance creation
        private ErrorLogger() { }

        // An LogWriter instance that exposes a single instance
        public static ErrorLogger Instance
        {
            get
            {
                // If the instance is null then create one and init the Queue
                if (instance == null)
                {
                    instance = new ErrorLogger();
                    logQueue = new Queue<ErrorLog>();
                }
                return instance;
            }
        }

        // The single instance method that writes to the log file
        public void WriteToLog(string message)
        {
            try
            {
                // Lock the queue while writing to prevent contention for the log file
                lock (logQueue)
                {
                    // Create the entry and push to the Queue
                    ErrorLog logEntry = new ErrorLog(message);
                    logQueue.Enqueue(logEntry);

                    // If we have reached the Queue Size then flush the Queue
                    if (logQueue.Count >= queueSize || DoPeriodicFlush())
                    {
                        FlushLog();
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private bool DoPeriodicFlush()
        {
            try
            {
                TimeSpan logAge = DateTime.Now - LastFlushed;
                if (logAge.TotalSeconds >= maxLogAge)
                {
                    LastFlushed = DateTime.Now;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }


        // Flushes the Queue to the physical log file
        private void FlushLog()
        {
            try
            {
                while (logQueue.Count > 0)
                {
                    ErrorLog entry = logQueue.Dequeue();
                    string logPath = logDir + entry.LogDate + "_" + logFile;

                    // This could be optimised to prevent opening and closing the file for each write
                    using (FileStream fs = File.Open(logPath, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter log = new StreamWriter(fs))
                        {
                            log.WriteLine(string.Format("{0}\t{1}", entry.LogTime, entry.Message));
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        // Flushes the Queue to the physical log file
        public void ManualFlushLog()
        {
            try
            {
                while (logQueue.Count > 0)
                {
                    ErrorLog entry;

                    lock (logQueue)
                    {
                        entry = logQueue.Dequeue();
                    }
                    string logPath = logDir + entry.LogDate + "_" + logFile;

                    // This could be optimised to prevent opening and closing the file for each write
                    using (FileStream fs = File.Open(logPath, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter log = new StreamWriter(fs))
                        {
                            log.WriteLine(string.Format("{0}\t{1}", entry.LogTime, entry.Message));
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }
    }//class ErrorLogger

    // A Log class to store the message and the Date and Time the log entry was created
    public class ErrorLog
    {
        public string Message { get; set; }
        public string LogTime { get; set; }
        public string LogDate { get; set; }

        public ErrorLog(string message)
        {
            Message = message;
            LogDate = DateTime.Now.ToString("yyyy-MM-dd");
            LogTime = DateTime.Now.ToString("hh:mm:ss.fff tt");
        }
    }//public class ErrorLog

}//namespace CQGMinuteServer
